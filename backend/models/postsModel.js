const db=require("../config/db")

const allPosts=async()=>{
    return db.findAll()
}

const post=async(id)=>{
    const data=await db.findAll({
        where:{
            id
        }
    })
    return data
}

const createPost=async(data)=>{
    const create=await db.create(data)
    return create
}

const updatePost=async(id,data)=>{
    const update=await db.update(data,{
        where:{
            id
        }
    })
    return update
}

const deletePost=async(id)=>{
    const deleteData=await db.destroy({
        where:{
            id
        }
    })
    return deleteData
}

module.exports={allPosts,post,createPost,updatePost,deletePost}