const express=require("express")
const postsModel=require("../models/postsModel")
const response=require("../config/message")
const router=express.Router()

router.get("/",async (req,res)=>{
    try {
        const posts=await postsModel.allPosts()
        res.json(posts)
    }
    catch(err){
        response.error(res,500)
    }
})

router.get("/:id",async (req,res)=>{
    try {
        const id_post=req.params.id
        const post=await postsModel.post(id_post)
        if(post.length>0)
            res.json(post)
        else
            response.error(res,404)
    }
    catch(err){
        response.error(res,500)
    }
})

router.post("/",async (req,res)=>{
    try {
        const data=req.body
        const imgLink=req.body.img
        const regexImg=/([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png))/i
        const validateImg=regexImg.test(imgLink)
        const emptyData=Object.values(data).some(value=>value==='')
        if(!emptyData && validateImg){
            await postsModel.createPost(data)
            response.success(res,"created",'',201)
        }
        else
            response.error(res,400)
    }
    catch(err){
        response.error(res,500)
    }
})

router.patch("/:id",async (req,res)=>{
    try {
        const id_post=req.params.id
        const data=req.body
        const result=await postsModel.updatePost(id_post,data)
        if(result[0])
            response.success(res,"update")
        else
            response.error(res,400)
    }
    catch(err){
        response.error(res,500)
    }
})

router.delete("/:id",async(req,res)=>{
    try {
        const id_post=req.params.id
        const result=await postsModel.deletePost(id_post)
        if(result)
            response.success(res,"delete")
        else
            response.error(res,404)
    }
    catch(err){
        response.error(res,500)
    }
})

module.exports=router