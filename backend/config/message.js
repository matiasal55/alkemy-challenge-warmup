const error=(res,status)=>{
    res.status(status).json({error:status})
}

const success=(res,action,message,status)=>{
    res.status(status || 200).json({[action]:message || "Ok"})
}

module.exports={success,error}