const Sequelize = require("sequelize");
require("dotenv").config();

const db_name = process.env.DB_NAME;
const db_user = process.env.DB_USER;
const db_password = process.env.DB_PASSWORD;
const db_host = process.env.DB_HOST;

const sequelize = new Sequelize(db_name, db_user, db_password, {
  host: db_host,
  dialect: "mysql",
});

const Posts = sequelize.define("Posts", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  title: Sequelize.STRING,
  content: Sequelize.TEXT,
  img: Sequelize.STRING,
  category: Sequelize.STRING,
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Connected to DB");
  })
  .catch((err) => {
    console.log("Connection error");
  });

module.exports = Posts;
