const express=require("express")
const postsRouter=require("./routes/posts")
const db=require("./config/db")
const app=express()

require("dotenv").config()

app.set("port",process.env.PORT)

app.use(express.json())
app.use(express.urlencoded({
    extended:false
}))
app.use("/posts",postsRouter)

app.listen(app.get("port"),()=>{
    console.log("Connected")
})