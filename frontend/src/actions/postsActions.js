const timeOutAlert = 1000;

export const getPosts = () => async (dispatch) => {
  dispatch({
    type: "loading",
  });
  fetch("https://jsonplaceholder.typicode.com/posts")
    .then((res) => res.json())
    .then((data) => {
      dispatch({
        type: "getPosts",
        payload: data,
      });
    })
    .catch((err) => {
      dispatch({
        type: "error",
        payload: "Error",
      });
    });
};

export const newPost = () => (dispatch) => {
  dispatch({
    type: "newPost",
  });
};

export const getPost = (id) => async (dispatch) => {
  dispatch({
    type: "loading",
  });
  fetch("https://jsonplaceholder.typicode.com/posts/" + id)
    .then((res) => res.json())
    .then((data) => {
      dispatch({
        type: "getPost",
        payload: data,
      });
    });
};

export const sendPost = (e, data, id = null) => async (dispatch) => {
  e.preventDefault();
  dispatch({
    type: "loading",
  });
  const params = {
    body: JSON.stringify({
      title: data.title,
      body: data.body,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  };
  if (id) params.method = "PATCH";
  else params.method = "POST";
  fetch(`https://jsonplaceholder.typicode.com/posts/${id || ""}`, params)
    .then((res) => {
      dispatch({
        type: "sendPost",
      });
    })
    .catch((err) => {
      dispatch({
        type: "errorSend",
      });
    })
    .finally(() => {
      showAlert(dispatch);
    });
};

export const deletePost = (id) => async (dispatch) => {
  dispatch({
    type: "loading",
  });
  fetch("https://jsonplaceholder.typicode.com/posts/" + id, {
    method: "DELETE",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  })
    .then((res) => {
      dispatch({
        type: "deletePostOk",
      });
    })
    .catch((err) => {
      dispatch({
        type: "deletePostError",
      });
    })
    .finally(() => {
      showAlert(dispatch);
    });
};

export const alertDefault = () => (dispatch) => {
  dispatch({
    type: "alertDefault",
  });
};

const showAlert = (dispatch) => {
  setTimeout(() => {
    dispatch({
      type: "alertDefault",
    });
  }, timeOutAlert);
};
