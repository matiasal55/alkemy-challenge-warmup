const INITIAL_STATE = {
  posts: [],
  loading: true,
  messageAlert: "",
  backgroundAlert: "",
  alert: false,
  post: {},
  redirect: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "loading":
      return { ...state, loading: true };
    case "getPosts":
      return {
        ...state,
        posts: action.payload,
        loading: false,
        redirect: false,
      };
    case "newPost":
      return { ...state, post: {}, loading: false };
    case "getPost":
      return { ...state, post: action.payload, loading: false };
    case "sendPost":
      return {
        ...state,
        redirect: true,
        messageAlert: "El post ha sido agregado con éxito",
        backgroundAlert: "success",
        alert: true,
        loading: false,
      };
    case "errorSend":
      return {
        ...state,
        redirect: true,
        loading: false,
        alert: true,
        messageAlert: "Hubo un error de posteo. Intente más tarde",
        backgroundAlert: "danger",
      };
    case "deletePostOk":
      return {
        ...state,
        messageAlert: "El post ha sido eliminado correctamente",
        backgroundAlert: "success",
        alert: true,
        loading: false,
      };
    case "deletePostError":
      return {
        ...state,
        messageAlert: "No se pudo eliminar el post",
        backgroundAlert: "danger",
        alert: true,
        loading: false,
      };
    case "alertDefault":
      return {
        ...state,
        alert: false,
      };
    default:
      return state;
  }
};
