import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import FormField from "./FormField";
import { connect } from "react-redux";
import * as postsActions from "../actions/postsActions";

class Form extends Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
  }

  redirectToHome() {
    if (this.props.redirect) return <Redirect to="/" />;
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  }

  componentDidMount() {
    this.setState({
      title: this.props.post.title || "",
      body: this.props.post.body || "",
    });
  }

  render() {
    return (
      <div>
        {this.redirectToHome()}
        <form
          className="was-validated"
          onSubmit={(e) =>
            this.props.sendPost(e, this.state, this.props.post.id)
          }
        >
          <FormField
            field="Título"
            type="text"
            name="title"
            value={this.props.post.title}
            onChange={this.handleChange}
          />
          <div className="form-group">
            <label>Texto</label>
            <textarea
              className="form-control"
              name="body"
              rows="3"
              defaultValue={this.props.post.body}
              onChange={this.handleChange}
            ></textarea>
          </div>
          <button className="btn btn-success">Publicar</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (reducers) => {
  return reducers.postsReducer;
};

export default connect(mapStateToProps, postsActions)(Form);
