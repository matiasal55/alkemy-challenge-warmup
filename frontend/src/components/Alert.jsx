import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import * as postsActions from "../actions/postsActions";

class Alert extends Component {
  render() {
    return (
      <>
        <div
          className={`alert alert-${this.props.backgroundAlert} alert-dismissible fade show mt-3`}
          role="alert"
        >
          {this.props.messageAlert}
          <button
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
            onClick={() => this.props.alertDefault()}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </>
    );
  }
}

const mapStateToProps = (reducers) => {
  return reducers.postsReducer;
};

export default connect(mapStateToProps, postsActions)(Alert);
