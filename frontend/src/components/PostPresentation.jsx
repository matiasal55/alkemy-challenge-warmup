import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class PostPresentation extends Component {
  render() {
    return (
      <tr>
        <th scope="row">
          <Link to={`/post/${this.props.id}`} onClick={this.props.view}>
            {this.props.title}
          </Link>
        </th>
        <td>
          <Link to="/edit" onClick={this.props.edit}>
            <i className="fas fa-edit"></i>
          </Link>
        </td>
        <td>
          <Link to="/" onClick={this.props.delete}>
            <i className="fas fa-trash-alt"></i>
          </Link>
        </td>
      </tr>
    );
  }
}
