import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import NavbarItem from './NavbarItem'
import { connect } from 'react-redux'
import * as postsActions from '../actions/postsActions'

class Navbar extends Component{
    render(){
    return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to="/">Challenge Frontend</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <NavbarItem link="/" title="Home"/>
                        <NavbarItem link="/new" action={()=>this.props.newPost()} title="Nuevo Post"/>
                    </ul>
                </div>
            </nav>
    )
    }
}

const mapStateToProps=(reducers)=>{
    return reducers.postsReducer
}

export default connect(mapStateToProps,postsActions)(Navbar)