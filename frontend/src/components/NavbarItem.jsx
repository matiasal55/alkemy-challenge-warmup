import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class NavbarItem extends Component{
    render(){
        return(
            <li className="nav-item">
                <Link className="nav-link" onClick={this.props.action} to={this.props.link}>{this.props.title}</Link>
            </li>
        )
    }
}