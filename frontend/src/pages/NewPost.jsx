import React, { Component } from "react";
import Form from "../components/Form";
import Loading from "../components/Loading";
import { connect } from "react-redux";
import * as postsActions from "../actions/postsActions";

class NewPost extends Component {
  render() {
    if (this.props.loading) return <Loading />;
    return <Form />;
  }
}

const mapStateToProps = (reducers) => {
  return reducers.postsReducer;
};

export default connect(mapStateToProps, postsActions)(NewPost);
