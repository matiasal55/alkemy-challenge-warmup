import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import * as postsActions from "../actions/postsActions";
import Loading from "../components/Loading";

class Post extends Component {
  render() {
    if (this.props.loading) return <Loading />;
    return (
      <Fragment>
        <h1>{this.props.post.title}</h1>
        <p>{this.props.post.body}</p>
      </Fragment>
    );
  }
}

const mapStateToProps = (reducers) => {
  return reducers.postsReducer;
};

export default connect(mapStateToProps, postsActions)(Post);
