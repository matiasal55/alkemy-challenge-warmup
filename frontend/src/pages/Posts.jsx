import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Alert from "../components/Alert";
import Loading from "../components/Loading";
import PostPresentation from "../components/PostPresentation";
import * as postsActions from "../actions/postsActions";

class Posts extends Component {
  showAlert() {
    return this.props.alert ? (
      <Alert
        messageAlert={this.props.messageAlert}
        backgroundAlert={this.props.backgroundAlert}
      />
    ) : (
      <></>
    );
  }

  showPosts() {
    return this.props.posts.map((post) => {
      return (
        <Fragment key={post.id}>
          <PostPresentation
            id={post.id}
            title={post.title}
            view={() => this.props.getPost(post.id)}
            edit={() => this.props.getPost(post.id)}
            delete={() => this.props.deletePost(post.id)}
          />
        </Fragment>
      );
    });
  }

  loadingData() {
    if (this.props.loading) return <Loading />;
    return (
      <table className="table mt-3">
        <thead>
          <tr>
            <th scope="col">Título</th>
            <th scope="col">Editar</th>
            <th scope="col">Eliminar</th>
          </tr>
        </thead>
        <tbody>{this.showPosts()}</tbody>
      </table>
    );
  }

  componentDidMount() {
    this.props.getPosts();
  }

  render() {
    return (
      <Fragment>
        {this.showAlert()}
        {this.loadingData()}
      </Fragment>
    );
  }
}

const mapStateToProps = (reducers) => {
  return reducers.postsReducer;
};

export default connect(mapStateToProps, postsActions)(Posts);
